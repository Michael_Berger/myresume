# Cloud Ops and Cloud Delivery Manager

The Cloud Delivery Manager is a key member of our rapidly growing DevOps group, responsible for the vision, strategy, execution and management of the Cloud Operations team. This includes building and maintaining a roadmap for the team, managing Agile project and ticket work, coaching team members and working directly with customers.


As the Cloud Delivery Manager, you’ll be involved in:
- Managing, evolving, and coaching the team of Cloud and DevOps Engineers and Architects, responsible for the delivery of customer projects, including:
    - Google Cloud and MS Azure cloud strategy and migrations
    - Application migrations, modernization of legacy applications, building infrastructure from code (Terraform)
    - Data Engineering and Analytics projects
- Customer relationship management, helping ensure the success of our Cloud projects and ultimately the success of the customer
- Business development and pre-sales activities, working Cloud and DevOps opportunities with the sales team and pre-sales engineers
- Building DevOps features and functionality to evolve our production applications and service offerings, including features on:
    - Google Cloud Platform
    - Kubernetes and GKE
    - StackDriver and ScienceLogic monitoring
    - Jenkins / CloudBees
    - Terraform, Ansible, Chef
    - APM
- Setting up Day 2 Operations for customers, including monitoring (StackDriver, ScienceLogic), logging (StackDriver, Elastic), and incident management (ServiceNow)
- Monitoring and troubleshooting issues of the DevOps group’s production applications, running on Google’s Kubernetes Engine (GKE)
- Leading all agile activities, including sprint planning, daily standups, sprint reviews and retrospectives.

## The Tech Stuff
- Experience with public cloud (Google Cloud, Azure, AWS)
- Experience in IT Operations or Software Management
- Infrastructure as code experience with Terraform, CloudFormation, etc.
- Scripting experience with Python, BASH, PowerShell
- Containers and Kubernetes experience a plus
- Configuration Management (e.g. Chef, Ansible, Puppet) experience a plus
